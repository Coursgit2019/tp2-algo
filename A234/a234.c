#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pile.h"
#include "file.h"
#include "a234.h"

#define max(a,b) ((a)>(b)?(a):(b))


int hauteur (Arbre234 a)
{
  int h0, h1, h2, h3 ;
  
  if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;

  h0 = hauteur (a->fils [0]) ;
  h1 = hauteur (a->fils [1]) ;
  h2 = hauteur (a->fils [2]) ;
  h3 = hauteur (a->fils [3]) ;

  return 1 + max (max (h0,h1),max (h2,h3)) ;
} 

int NombreCles (Arbre234 a)
{

  pfile_t f=creer_file();
  enfiler(f,a);
  
  int i=0;
  
  while(!(file_vide(f))){
    
    Arbre234 tmp=defiler(f);
    
    if(tmp->t == 2){
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[2]);
      i++ ;
    }
    if(tmp->t == 3){
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[0]);
      i = i + 2 ;
    }
    if(tmp->t == 4){
      enfiler(f,tmp->fils[3]);
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[0]);
      i = i + 3 ;
    }
  }
  return i;

}

int CleMax (Arbre234 a)
{
  int toReturn = 0;
  if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;

  while (a->t != 0){

    if (a->t == 2)
    {
      toReturn = a->cles[1] ;

      a =  a-> fils[2];
    }

  if (a->t == 3)
    {
      toReturn = a->cles[1] ;

      a =  a-> fils[2];
    }

  if (a->t == 4)
    {
      toReturn = a->cles[2] ;

      a =  a-> fils[3];

    }
  }

  return toReturn;
}

int CleMin (Arbre234 a)
{
  int toReturn = 0;
  if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;

  while (a->t != 0){

    if (a->t == 2)
    {
      toReturn = a->cles[1] ;

      a =  a-> fils[1];
    }

  if (a->t == 3)
    {
      toReturn = a->cles[0] ;

      a =  a-> fils[0];
    }

  if (a->t == 4)
    {
      toReturn = a->cles[0] ;

      a =  a-> fils[0];

    }
  }

  return toReturn;
}

Arbre234 RechercherCle (Arbre234 a, int cle)
{
 int pos ;
 if (a == NULL) {return NULL ;}
  

  if(a->t == 2){
      if(cle == a-> cles[1]){
        return a;
      }
      if(cle < a-> cles[1]){
        a = a->fils[1];
        RechercherCle (a, cle);
      }
      if(cle > a-> cles[1]){
        a = a->fils[2];
        RechercherCle (a, cle);
      }
  }

  if(a->t == 3){
      if(cle == a-> cles[0]){
        return a;
      }
      if(cle == a-> cles[1]){
        return a;
      }
      if(cle < a-> cles[0]){
        a = a->fils[0];
        RechercherCle (a, cle);
      }
      if(cle > a-> cles[0] && cle < a-> cles[1]){
        a = a->fils[1];
        RechercherCle (a, cle);
      }

      if(cle > a-> cles[1]){
        a = a->fils[2];
        RechercherCle (a, cle);
      }
  }
    if(a->t == 4){
      if(cle == a-> cles[0]){

        return a;
      }
      if(cle == a-> cles[1]){
        return a;
      }
      if(cle == a-> cles[2]){
        return a;
      }

      if(cle < a-> cles[0]){
        a = a->fils[0];
        RechercherCle (a, cle);
      }
      if(cle > a-> cles[0] && cle < a-> cles[1]){
        a = a->fils[1];
        RechercherCle (a, cle);
      }

      if(cle > a-> cles[1] && cle < a-> cles[2]){
        a = a->fils[2];
        RechercherCle (a, cle);
      }

      if(cle > a-> cles[2]){
        a = a->fils[3];
        RechercherCle (a, cle);
      }

    }

  return NULL ;
}


void AnalyseStructureArbre (Arbre234 a, int *feuilles, int *noeud2, int *noeud3, int *noeud4)
{
    if (a == NULL)
      return ;

    if (a->t == 0){

      *feuilles = *feuilles + 1 ;

    }

    if (a->t == 2)
    {
      *noeud2 = *noeud2+1;

      AnalyseStructureArbre (a->fils[2], feuilles,  noeud2, noeud3, noeud4) ;

      AnalyseStructureArbre (a->fils[1], feuilles, noeud2, noeud3, noeud4) ;

    }

  if (a->t == 3)
    {
      *noeud3 = *noeud3+1;

      AnalyseStructureArbre (a->fils[2], feuilles, noeud2, noeud3, noeud4) ;
      
      AnalyseStructureArbre (a->fils[1], feuilles, noeud2, noeud3, noeud4) ;

      AnalyseStructureArbre (a->fils[0], feuilles, noeud2, noeud3, noeud4) ;
    }

  if (a->t == 4)
    {
      *noeud4 = *noeud4+1;

      AnalyseStructureArbre (a->fils[3],feuilles, noeud2, noeud3, noeud4) ;

      AnalyseStructureArbre (a->fils[2],feuilles, noeud2, noeud3, noeud4) ;
      
      AnalyseStructureArbre (a->fils[1],feuilles, noeud2, noeud3, noeud4) ;

      AnalyseStructureArbre (a->fils[0],feuilles, noeud2, noeud3, noeud4) ;
    }

    return;
}

Arbre234 noeud_max (Arbre234 a)
{
  if(a==NULL)
    {
      return 0;
    }

  pfile_t f=creer_file();
  enfiler(f,a);
  int i = 0;
  int index = 0;
  int sum = 0;
  Arbre234 res = malloc(sizeof(noeud234));
  while(!(file_vide(f))){
    Arbre234 tmp=defiler(f);
    if(tmp->t == 2){
      if(i<tmp->cles[1]){
        i=tmp->cles[1];
        res->t = tmp->t;
        res->cles[1] = tmp->cles[1]; 
        res->fils[1] = tmp->fils[1];
        res->fils[2] = tmp->fils[2];
      }
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[1]);
    }
    if(tmp->t == 3){
      sum = tmp->cles[0]+tmp->cles[1];
      if(i<sum){
        i=sum;
        res->t = tmp->t;
        res->cles[0] = tmp->cles[0];
        res->cles[1] = tmp->cles[1];
        res->fils[0] = tmp->fils[0];
        res->fils[1] = tmp->fils[1];
        res->fils[2] = tmp->fils[2];
      }
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[0]);
    }
    if(tmp->t == 4){
      sum = tmp->cles[0]+tmp->cles[1]+tmp->cles[2];
      if(i<sum){
        i=sum;
        res->t = tmp->t;
        res->cles[0] = tmp->cles[0];
        res->cles[1] = tmp->cles[1];
        res->cles[2] = tmp->cles[2];
        res->fils[0] = tmp->fils[0];
        res->fils[1] = tmp->fils[1];
        res->fils[2] = tmp->fils[2];
        res->fils[3] = tmp->fils[3];
      }
      enfiler(f,tmp->fils[3]);
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[0]);
    }
  }
  return res;
}


void Afficher_Cles_Largeur (Arbre234 a)
{

  pfile_t f=creer_file();
  enfiler(f,a);
  while(!(file_vide(f))){
    Arbre234 tmp=defiler(f);
    if(tmp->t == 2){
      printf("%d \n",tmp->cles[1]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[2]);
    }
    if(tmp->t == 3){
      printf("%d \n",tmp->cles[0]);
      printf("%d \n",tmp->cles[1]);
      enfiler(f,tmp->fils[0]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[2]);
    }
    if(tmp->t == 4){
      printf("%d \n",tmp->cles[0]);
      printf("%d \n",tmp->cles[1]);
      printf("%d \n",tmp->cles[2]);
      enfiler(f,tmp->fils[0]);
      enfiler(f,tmp->fils[1]);
      enfiler(f,tmp->fils[2]);
      enfiler(f,tmp->fils[3]);
    }

  }
  return ;
}

void Affichage_Cles_Triees_Recursive (Arbre234 a)
{
  if(a==NULL){return ;}
  else{
    Arbre234 tmp= a;
    if(tmp->t == 2){

      Affichage_Cles_Triees_Recursive(a->fils[0]);
      printf("%d \n",a->cles[1]);
      Affichage_Cles_Triees_Recursive(a->fils[1]);
    }
    if(tmp->t == 3){

      Affichage_Cles_Triees_Recursive(a->fils[0]);
      printf("%d \n",a->cles[0]);
      Affichage_Cles_Triees_Recursive(a->fils[1]);
      printf("%d \n",a->cles[1]);
      Affichage_Cles_Triees_Recursive(a->fils[2]);
    }
    if(tmp->t == 4){
      Affichage_Cles_Triees_Recursive(a->fils[0]);
      printf("%d \n",a->cles[0]);
      Affichage_Cles_Triees_Recursive(a->fils[1]);
      printf("%d \n",a->cles[1]);
      Affichage_Cles_Triees_Recursive(a->fils[2]);
      printf("%d \n",a->cles[2]);
      Affichage_Cles_Triees_Recursive(a->fils[3]);
    }
    return ;
  }
     
}

void Affichage_Cles_Triees_NonRecursive (Arbre234 a)
{
  /*
    if (self != NULL)
    {
        Pile *pile = pile_init();
        int end = 0;
        Arbre *noeud = self;
 
        while(!end)
        {
            while (noeud != NULL)
            {
                pile_empiler(&pile, noeud);
                noeud = noeud->FilsG;
            }
 
            if (pile_estVide(pile))
            {
                end = 1;
            }
            else
            {
                noeud = pile_depiler(&pile);
                printf("%d ", noeud->val);
                noeud = noeud->FilsD;
            }
        }
        printf("\n");
    }
        */

  if(a==NULL){return ;}  
  ppile_t p=creer_pile();
  int fin =0;
  Arbre234 tmp=a;
  int index = 0;

  while(!fin){
    while (tmp->t != 0){
      empiler(p,tmp);
      if(tmp->t==2)
        tmp = tmp->fils[index+1];     
    }
    if(pile_vide(p)){
      fin = 1;
    }
    else{
      tmp = depiler(p);
      if(tmp->t == 2){
        printf("%d \n",tmp->cles[1]);
      }
      if(tmp->t == 3){
        printf("%d \n",tmp->cles[0]);
        printf("%d \n",tmp->cles[1]);
      }
      if(tmp->t == 4){       
        printf("\n");
        printf("%d \n",tmp->cles[0]);
        printf("%d \n",tmp->cles[1]);
        printf("%d \n",tmp->cles[2]);
      }
      index = index + 1;
      tmp = tmp->fils[index];
    }
    printf("\n");
  }

  
  return ;

}


void Detruire_Cle (Arbre234 *a, int cle)
{
  /*
    retirer la cle de l'arbre a
  */

  return ;
}




int main (int argc, char **argv)
{
  Arbre234 a ;

  Arbre234 resResearch = NULL;

  int res = 0;

  int feuille = 0;
  int noeud2 = 0;
  int noeud3 = 0;
  int noeud4 = 0;

  if (argc != 2)
    {
      fprintf (stderr, "il manque le parametre nom de fichier\n") ;
      exit (-1) ;
    }

  a = lire_arbre (argv [1]) ;

  printf ("==== Afficher arbre ====\n") ;
  
  afficher_arbre (a, 0) ;

  printf ("==== Test Nombre cles ====\n") ;

  res = NombreCles(a);

  printf ("%d \n", res);

  printf ("==== Test Cle Max ====\n") ;

  res = CleMax(a);

  printf ("%d \n", res);

  printf ("==== Test Cle Min ====\n") ;

  res = CleMin(a);

  printf ("%d \n", res);

  printf ("==== Test Rechercher Cle ====\n") ;

  resResearch = RechercherCle(a, 50);

  if(resResearch != NULL){

    printf ("La cle 50 est presente\n") ;

  }
  else{

    printf ("La cle 50 n'est pas presente\n") ;

  }

  resResearch = RechercherCle(a, 300);

  if(resResearch != NULL){

    printf ("La cle 300 est presente\n") ;

  }
  else{

    printf ("La cle 300 n'est pas presente\n") ;

  }

  printf ("==== Test Analyse structure Arbre ====\n") ;

  AnalyseStructureArbre(a, &feuille, &noeud2, &noeud3, &noeud4);

  printf (" il y a %d feuilles \n", feuille);

  printf (" il y a %d noeud2 \n", noeud2);

  printf (" il y a %d noeud3 \n", noeud3);

  printf (" il y a %d noeud4 \n", noeud4);

  printf ("==== Test Noeud Max ====\n") ;

  resResearch = noeud_max(a);

  if(resResearch->t == 2){
    printf("%d \n",resResearch->cles[1]);
  }
  if(resResearch->t == 3){
    printf("%d \n",resResearch->cles[0] + resResearch->cles[1]);
  }
  if(resResearch->t == 4){
    printf("%d \n",resResearch->cles[0] + resResearch->cles[1] + resResearch->cles[2]);
  }

  printf ("==== Test Afficher Cles Largeur ====\n") ;

  Afficher_Cles_Largeur(a);

  printf ("==== Test Afficher Cles Triees Recursive ====\n") ;

  Affichage_Cles_Triees_Recursive(a);

  printf ("==== Test Afficher Cles Triees Non Recursive ====\n") ;

  Affichage_Cles_Triees_NonRecursive(a);

  printf ("==== Test Detruire Cle ====\n") ;

}
