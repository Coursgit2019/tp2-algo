#include <stdio.h>
#include <stdlib.h>

#include "file.h"


int main (int argc, char**argv)
{
  pfile_t p ;
  int res;
  pnoeud_t n;

  n = (Arbre_t) malloc (sizeof(noeud_t)) ;
  n->cle = 15;
  n->fgauche = NULL ;
  n->fdroite = NULL ;

  p = creer_file() ;
  res = file_vide(p);

  printf ("************************premier test file vide************************\n \n") ;

  if(res==1)
    printf ("La file est vide \n ") ;
  else
    printf ("La file n'est pas vide\n ") ;
 
  printf ("************************apres enfiler************************\n \n") ; 

  res = enfiler(p,n);
  
  res = file_vide(p);
  if(res==1)
    printf ("La file est vide \n ") ;
  else
    printf ("La file n'est pas vide\n ") ;

  printf ("************************apres defiler************************\n \n") ;
  defiler(p);
  
  res = file_vide(p);
  if(res==1)
    printf ("La file est vide \n ") ;
  else
    printf ("La file n'est pas vide\n ") ;

  printf ("************************test file pleine 1************************\n \n") ;
  
  res = file_pleine(p);
  if(res==1)
    printf ("La file est pleine \n ") ;
  else
    printf ("La file n'est pas pleine\n ") ;

  printf ("************************test file pleine 2************************\n \n") ;
  
  res = file_pleine(p);
  while (res != 1){
    res = enfiler(p,n);
    res = file_pleine(p);
  }

  if(res==1)
    printf ("La file est pleine \n ") ;
  else
    printf ("La file n'est pas pleine\n ") ;

  res = detruire_file(p);
}