#include <stdio.h>
#include <stdlib.h>

#include "pile.h"
#include "abr.h"

int main (int argc, char**argv)
{
  ppile_t p ;
  int res;
  pnoeud_t n;

  n = (Arbre_t) malloc (sizeof(noeud_t)) ;
  n->cle = 15;
  n->fgauche = NULL ;
  n->fdroite = NULL ;

  p = creer_pile() ;
  res = pile_vide(p);

  printf ("************************premier test pile vide************************\n \n") ;

  if(res==1)
    printf ("La pile est vide \n ") ;
  else
    printf ("La pile n'est pas vide\n ") ;
 
  printf ("************************apres empiler************************\n \n") ; 

  empiler(p,n);
  
  res = pile_vide(p);
  if(res==1)
    printf ("La pile est vide \n ") ;
  else
    printf ("La pile n'est pas vide\n ") ;

  printf ("************************apres depiler************************\n \n") ;
  depiler(p);
  
  res = pile_vide(p);
  if(res==1)
    printf ("La pile est vide \n ") ;
  else
    printf ("La pile n'est pas vide\n ") ;

  printf ("************************test pile pleine 1************************\n \n") ;
  
  res = pile_pleine(p);
  if(res==1)
    printf ("La pile est pleine \n ") ;
  else
    printf ("La pile n'est pas pleine\n ") ;

  printf ("************************test pile pleine 2************************\n \n") ;
  
  res = pile_pleine(p);
  while (res != 1){
    empiler(p,n);
    res = pile_pleine(p);
  }

  if(res==1)
    printf ("La pile est pleine \n ") ;
  else
    printf ("La pile n'est pas pleine\n ") ;

  res = detruire_pile(p);
}