#include <stdlib.h>
#include "a234.h"
#include "pile.h"

ppile_t creer_pile ()
{
  ppile_t pointeur;

  pointeur = (ppile_t) malloc(sizeof(pile_t));
 
  pointeur->sommet= 0;

  return pointeur;
}

int detruire_pile (ppile_t p)
{
  free(p);
  return 0;
}  

int pile_vide (ppile_t p)
{
   if (p->sommet== 0)
      return 1;
   return 0;
}

int pile_pleine (ppile_t p)
 {
   if (p->sommet== MAX_PILE_SIZE-1){
      return 1;
   }
   return 0;
} 

pnoeud234 depiler (ppile_t p)
{
  if(!pile_vide(p)){
  pnoeud234 elementDepile = p->Tab[p->sommet];
  pnoeud234 Retourner = malloc(sizeof(noeud234));
  Retourner->t = elementDepile->t;
  if (elementDepile->t == 2){
    Retourner->cles[1] = elementDepile->cles[1];
    Retourner->fils[0] = elementDepile->fils[0];
    Retourner->fils[1] = elementDepile->fils[1];
  }
  if (elementDepile->t == 3){
    Retourner->cles[0] = elementDepile->cles[0];
    Retourner->cles[1] = elementDepile->cles[1];
    Retourner->fils[0] = elementDepile->fils[0];
    Retourner->fils[1] = elementDepile->fils[1];
    Retourner->fils[2] = elementDepile->fils[2];
  }
  if (elementDepile->t == 4){
    Retourner->cles[0] = elementDepile->cles[0];
    Retourner->cles[1] = elementDepile->cles[1];
    Retourner->cles[2] = elementDepile->cles[2];
    Retourner->fils[0] = elementDepile->fils[0];
    Retourner->fils[1] = elementDepile->fils[1];
    Retourner->fils[2] = elementDepile->fils[2];
    Retourner->fils[3] = elementDepile->fils[3];
  }

  p->sommet--;
  free(elementDepile);
  return Retourner;
  }
  return NULL;
}

int empiler (ppile_t p, pnoeud234 pn)
{
  if(pile_vide(p)){
    p->Tab[p->sommet] = pn;
  }
  if(!pile_pleine(p)){
    p->sommet++;
    p->Tab[p->sommet] = pn;
  }
  return 0;
}
