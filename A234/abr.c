#include <stdio.h>

#include <stdlib.h>

#include "pile.h"
#include "file.h"


#define max(a,b) ((a)>(b)?(a):(b))

/*

int feuille (Arbre_t a)
{
  if (a == NULL)
    return 0 ;
  else
    {
      if ((a->fgauche == NULL) && (a->fdroite == NULL))
	return 1 ;
      else
	return 0 ;
    }
}

Arbre_t ajouter_noeud (Arbre_t a, Arbre_t n)
{
   ajouter le noeud n dans l'arbre a 

  if (a == NULL)
    return n ;
  else if (n->cle < a->cle)
	a->fgauche = ajouter_noeud (a->fgauche, n) ;
  else
	a->fdroite = ajouter_noeud (a->fdroite, n) ;
  return a ;

}

Arbre_t rechercher_cle_arbre (Arbre_t a, int valeur)
{
  if (a == NULL)
    return NULL ;
  else
    {
      if (a->cle == valeur)
	return a ;
      else
	{
	  if (a->cle < valeur)
	    return rechercher_cle_arbre (a->fdroite, valeur) ;
	  else
	    return rechercher_cle_arbre (a->fgauche, valeur) ;
	}
    }
}

Arbre_t ajouter_cle (Arbre_t a, int cle)
{
  Arbre_t n ;
  Arbre_t ptrouve ;

  /*
     ajout de la clé. Creation du noeud n qu'on insere
    dans l'arbre a


  ptrouve = rechercher_cle_arbre (a, cle) ;

  if (ptrouve == NULL)
    {
      n = (Arbre_t) malloc (sizeof(noeud_t)) ;
      n->cle = cle;
      n->fgauche = NULL ;
      n->fdroite = NULL ;

      a = ajouter_noeud (a, n) ;
      return a ;
    }
  else
    return a ;
}


Arbre_t lire_arbre (char *nom_fichier)
{
  FILE *f ;
  int cle;
  Arbre_t a = NULL;

  f = fopen (nom_fichier, "r") ;

  while (fscanf (f, "%d", &cle) != EOF)
    {
      a = ajouter_cle (a, cle) ;
    }

  fclose (f) ;

  return a ;
}

void afficher_arbre (Arbre_t a, int niveau)
{
  
    affichage de l'arbre a
    on l'affiche en le penchant sur sa gauche
    la partie droite (haute) se retrouve en l'air
  

  int i ;

  if (a != NULL)
      {
	afficher_arbre (a->fdroite,niveau+1) ;

	for (i = 0; i < niveau; i++)
	  printf ("\t") ;
	printf (" %d (%d)\n\n", a->cle, niveau) ;

	afficher_arbre (a->fgauche, niveau+1) ;
      }
  return ;
}


int hauteur_arbre_r (Arbre_t a)
{
  if((a->fgauche==NULL) && (a->fdroite==NULL)){return 0;}
  else{
    int hg=hauteur_arbre_r(a->fgauche);
    int hd=hauteur_arbre_r(a->fdroite);
    return(1+max(hg,hd));
  }

}

int hauteur_arbre_nr (Arbre_t a)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  int i=1;
  while(!(file_vide(f))){

    Arbre_t tmp=defiler(f);
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}
    if((tmp->fgauche!=NULL) || (tmp->fdroite!=NULL)){i++;}
  }
  return i;
}


void parcourir_arbre_largeur (Arbre_t a)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  while(!(file_vide(f))){
    Arbre_t tmp=defiler(f);
    //traiter tmp
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}

  }
  return ;
}

int nombre_cles_arbre_r (Arbre_t a)
{
  if((a->fgauche==NULL) && (a->fdroite==NULL)){return 1;}
  else{
    return(1+nombre_cles_arbre_r(a->fgauche)+nombre_cles_arbre_r(a->fdroite));
  }
}

int nombre_cles_arbre_nr (Arbre_t a)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  int i=0;
  while(!(file_vide(f))){
    i++;
    Arbre_t tmp=defiler(f);
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}

  }
  return i;
}

void imprimer_liste_cle_triee_r (Arbre_t a)
{

  if(a==NULL){return ;}
  else{
    printf("%d",a->cle);
    imprimer_liste_cle_triee_r(a->fgauche);
    imprimer_liste_cle_triee_r(a->fdroite);
    return ;
  }
}

void imprimer_liste_cle_triee_nr (Arbre_t a)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  while(!(file_vide(f))){
    Arbre_t tmp=defiler(f);
    printf("%d",tmp->cle);
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}

  }
  return ;
}


int arbre_plein (Arbre_t a)
{
  int n=nombre_cles_arbre_nr(a);
  int h=hauteur_arbre_nr(a);
  return((2^(h-1))==n);
}


Arbre_t rechercher_cle_sup_arbre (Arbre_t a, int valeur)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  Arbre_t proche;
  proche->cle=2147483646;
  while(!(file_vide(f))){
    Arbre_t tmp=defiler(f);
    if(tmp->cle>valeur && tmp->cle<proche->cle){
      proche=tmp;
    }
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}

  }
  if(proche->cle==2147483646){
    return(NULL);
  }
  else{
    return(proche);
  }
}

Arbre_t rechercher_cle_inf_arbre (Arbre_t a, int valeur)
{
  pfile_t f=creer_file();
  enfiler(f,a);
  Arbre_t proche;
  proche->cle=0;
  while(!(file_vide(f))){
    Arbre_t tmp=defiler(f);
    if(tmp->cle<valeur && tmp->cle>proche->cle){
      proche=tmp;
    }
    if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
    if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}

  }
  if(proche->cle==0){
    return(NULL);
  }
  else{
    return(proche);
  }
}


Arbre_t detruire_cle_arbre (Arbre_t a, int cle)
{
Arbre_t res=a;

pfile_t f=creer_file();
enfiler(f,a);
while(!(file_vide(f))){
  Arbre_t tmp=defiler(f);
  if(tmp->fgauche->cle==cle){tmp->fgauche=NULL;break;}
  if(tmp->fdroite->cle==cle){tmp->fdroite=NULL;break;}
  if(tmp->fgauche!=NULL){enfiler(f,tmp->fgauche);}
  if(tmp->fdroite!=NULL){enfiler(f,tmp->fdroite);}
}
return(res);
}

*/
