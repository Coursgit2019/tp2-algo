#include <stdlib.h>

#include "a234.h"
#include "file.h"

pfile_t creer_file ()
{
  pfile_t pointeur = malloc(sizeof(file_t));
 
  pointeur->queue= 0;
  pointeur->tete= 0;

  return pointeur;
}

int detruire_file (pfile_t f)
{
  free(f);
  return 0 ;
}


int file_vide (pfile_t f)
{
   if (f->tete == f->queue)
      return 1;
   return 0;
}

int file_pleine (pfile_t f)
  {
   if (f->tete== 0 && f->queue== MAX_FILE_SIZE)
      return 1;
   return 0;
}

pnoeud234 defiler (pfile_t f)
{
    pnoeud234 elementDefile;
    
    if (f->tete != f->queue)
      {
	elementDefile = f->Tab[f->tete] ;
	f->tete = (f->tete + 1) % MAX_FILE_SIZE ;
      }
    return elementDefile;
}

int enfiler (pfile_t f, pnoeud234 p)
{

  if (((f->queue + 1) % MAX_FILE_SIZE) != f->tete)
    {
      f->Tab [f->queue] = p ;
      f->queue = (f->queue + 1) % MAX_FILE_SIZE ;
    }
  return 0 ;
}
